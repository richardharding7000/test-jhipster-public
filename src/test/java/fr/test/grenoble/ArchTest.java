package fr.test.grenoble;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {

        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("fr.test.grenoble");

        noClasses()
            .that()
                .resideInAnyPackage("fr.test.grenoble.service..")
            .or()
                .resideInAnyPackage("fr.test.grenoble.repository..")
            .should().dependOnClassesThat()
                .resideInAnyPackage("..fr.test.grenoble.web..")
        .because("Services and repositories should not depend on web layer")
        .check(importedClasses);
    }
}
