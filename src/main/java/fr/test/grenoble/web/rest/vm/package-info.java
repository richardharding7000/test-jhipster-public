/**
 * View Models used by Spring MVC REST controllers.
 */
package fr.test.grenoble.web.rest.vm;
